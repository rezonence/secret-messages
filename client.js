(() => {

    const portKey = "port";
    const encryptedMessageElementId = "encryptedMessage";
    const questionElementId = "question";
    const answersElementId = "answers";
    const answerClassName = "answer";
    const messageElementId = "message";
    const rootElementId = "secretMessages";
    const questionAndAnswersElementId = "questionAndAnswers";
    const resultElementId = "result";
    const wrongAnswerMessage = "Wrong answer!";
    const messageTitleElementId = "messageTitle";
    function getPort() {
        return new URL(location).searchParams.get(portKey);
    }

    function getEndpoint() {
        return `http://localhost:${getPort()}`;
    }

    /**
     * Gets a promise of the encrypted secret message along with the question, possible answers and keys relating to each answer.
     * e.g.
     * {
     *     encryptedMessage: "foo",
     *     question: "bar?",
     *     answers: [
     *         {
     *             text: "yes",
     *             key: "key1"
     *         },
     *         {
     *             text: "no",
     *             key: "key2"
     *         }
     *     ]
     * }
     * @return {Promise<EncryptedData>}
     */
    async function getEncryptedData() {
        const endpointPrefix = getEndpoint();
        const messageEndpoint = `${endpointPrefix}/message`;
        try {
            const response = await fetch(messageEndpoint);
            return response.json();
        } catch (err) {
            throw new Error(`Couldn't contact server on port ${getPort()} (${err}). Is your server running?`);

        }

    }

    /**
     *
     * @param decryptionRequest - The request containing the encrypted message and key e.g.
     *
     * {
     *     encryptedMessage: "foo",
     *     key: "key1"
     * }
     *
     * @return {Promise<any>} A promise of the decrypted message object e.g.
     *
     * {
     *     message: "The answer is 42"
     * }
     *
     */
    async function decryptMessage(decryptionRequest) {
        const decryptionResource = `${getEndpoint()}/decrypt`;
        const response = await fetch(decryptionResource, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(decryptionRequest)
        });

        if(response.status !== 200) {
            throw new Error(wrongAnswerMessage);
        } else {
            return response.json();
        }

    }

    function clearElement(element) {
        element.parentNode.removeChild(element);
    }

    function handleError(error) {
        alert(error.message);
    }


    async function start() {
        try {
            const port = getPort();
            if (!port) {
                handleError(new Error(`Please use ?${portKey}=1234 if you're running your server on port 1234`));
            } else {
                const encryptedData = await getEncryptedData();
                document.getElementById(rootElementId).style.display = "block";
                const encryptedMessage = encryptedData.encryptedMessage;
                document.getElementById(encryptedMessageElementId).innerText = encryptedMessage;
                const questionElement = document.getElementById(questionElementId);
                questionElement.innerText = encryptedData.question;
                const answersElement = document.getElementById(answersElementId);
                for (const answer of encryptedData.answers) {
                    const answerText = answer.text;
                    const key = answer.key;
                    const answerElement = document.createElement("div");
                    answersElement.appendChild(answerElement);
                    answerElement.classList.add(answerClassName);
                    answerElement.innerText = answerText;
                    answerElement.onclick = async () => {
                        const questionAndAnswersElement = document.getElementById(questionAndAnswersElementId);
                        clearElement(questionAndAnswersElement);
                        document.getElementById(resultElementId).style.display = "block";
                        const decryptionRequest = {
                            key,
                            encryptedMessage
                        };
                        try {
                            const decryptedData = await decryptMessage(decryptionRequest);
                            document.getElementById(messageElementId).innerText = decryptedData.message;
                        } catch (err) {
                            if (err.message === wrongAnswerMessage) {
                                document.getElementById(messageTitleElementId).innerText = wrongAnswerMessage;
                            } else {
                                handleError(new Error(`Failed to decrypt messsage: ${err.message}`));
                            }
                        }
                    };
                }
            }

        } catch (err) {
            handleError(err);
        }
    }



    start();
})();
