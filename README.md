Rezonence Developer Challenge
=============================

Secret Messages
---------------

![Encryption machine](http://cbsnews2.cbsistatic.com/hub/i/r/2015/04/10/c3ea0db6-0f94-4984-9a82-3d6f25dc5fd6/crop/620x698+0+74/37486d76e11504eae57459cfdfcf77bd/enigma-machine.jpg)

### Overview
 1. A secret message is stored on your server
 2. An encrypted version is available to anyone, but they don't know how it's been encrypted.
 3. There's a way of asking to decrypt an encrypted message, with a key
 4. There is also a multiple-choice question that only the good guys can answer.
    The user will get a key for each answer, but **only one** unlocks the secret message!


### Implementation
You can use the provided web page as your front-end if you wish and write a compatible RESTful API using a language/framework of your choice.

#### Server side
Your API might have:

* A hard-coded secret message (or one selected from a pre-defined list) and multiple-choice question.
* The ability to generate keys for correct answers i.e. not hard-coded ones
* A message resource (`GET /message`) that returns an encrypted secret message along with a question, possible answers to the question and a key corresponding to each answer e.g.

Response:
```json
{
  "encryptedMessage": "foo123",
  "question": "Are you a good guy?",
  "answers": [
    {
      "text": "yes",
      "key": "bar1"
    },
    {
      "text": "no",
      "key": "bar2"
    },
    {
      "text": "maybe",
      "key": "bar3"
    }
  ]
}
```
 
* A decrypting resource (`POST /decrypt`) that takes a key and an encrypted message and returns the secret message (or a failure) e.g.

Request:
```json
{
  "encryptedMessage": "foo123",
  "key": "bar1"
}
```
Response:
```json
{
  "message": "Meet at 7 dials"
}
```

#### Client side
 * You can load the [index.html](index.html) file in a modern browser and append the port of your API server as a query parameter e.g. `http://localhost:9000/index.html?port=3000` if you're hosting the site on port 9000 and your API on port 3000.
 * Feel free to adapt the provided code in any way you wish as long as you end up with something (web-based) that enables the general flow outlined in the [overview section](#Overview)!
 * Ideally it's simple to use, but makes it hard / impossible to cheat...

### Optional extras
If you find that was too easy or boring (!!), you can try _one_ of these, but you don't need to. People have real lives too.

#### The Dirty Harry version
![Do you feel lucky](http://4.bp.blogspot.com/-AqZZ2pGMii8/UhXAaEWT9AI/AAAAAAAADbw/4fXf6OQlIWs/s1600/do-you-feel-lucky-punk1.png)

 * The user only has one guess.
 * After that, all keys will fail to decrypt the message. **Do you feel lucky?**


#### The Mission Impossible version
![_This message will self-destruct in five seconds_](https://i.giphy.com/media/gDZnwG4jvJQNG/200w_d.gif)

 * The user only has five (or some number) seconds to answer the question!
 * After that, all the keys are no longer valid.
 * ...but the user can try another question (even if it's the same actual question for now)


Recommendations
---------------

### General
 * Don't worry too much about the question(s). Perhaps it should be hard for ~~the bad guys~~ _a machine_ to guess but easy for a human...

### Technical
 * Use whatever language or frameworks you see fit. We're pretty flexible.
 * We need to be able to run / test this in some way -- please outline this process.
 * Please publish your repo privately (allowing us access of course), or zip the contents of your codebase (including any `.git` or equivalent files - this is interesting stuff) and email it across to [careers@rezonence.com](careers@rezonence.com). Do not leave it on Github for all to see!

### We are looking for

 * How you go about solving the unusual problem at hand, and how you break this down into chunks.
 * A reasonable understanding of RESTful APIs and clients.
 * We're _not_ interested in rigorous cryptography or the like (erm, unless you _really_ want).
 * Demonstration of some automated testing
 * Clean, modern coding in a style that you like